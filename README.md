# Window manager
Destination: https://jvirkovskiy@bitbucket.org/2rl/unity-lib-window-manager.git  
Dependencies:
    "2reallife.base.logicality": "https://bitbucket.org/2rl/unity-lib-logicality.git?path=/Assets/Scripts/Base#2.0.0",
    "2reallife.helpers.touch-helper": "https://bitbucket.org/2rl/unity-lib-touch-helper.git?path=/Assets/Scripts/Helpers/TouchHelper#1.0.0",
    "2reallife.base.window-manager": "https://bitbucket.org/2rl/unity-lib-window-manager.git?path=/Assets/Scripts/Base/WindowManager#2.0.0"

See https://2rll.atlassian.net/wiki/spaces/MC/pages/855322/WindowManager for details.
