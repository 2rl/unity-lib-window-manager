using System;
using System.Collections.Generic;

namespace Base.WindowManager
{
	public interface IWindowManager
	{
		/// <summary>
		/// Открыть окно.
		/// </summary>
		/// <param name="windowId">Идентификатор открываемого окна.</param>
		/// <param name="args">Аргументы, передаваемые окну при открытии.</param>
		/// <param name="isUnique">Флаг, указывающий на то, что окно должно быть показано эксклюзивно.
		/// Если значение отсутствует, используется значение, возвращаемое окном.</param>
		/// <param name="overlap">Флаг, указывающий на то, что на время показа окна, предыдущее окно должно
		/// быть скрыто. Если значение отсутствует, используется значение, возвращаемое окном.</param>
		/// <param name="windowGroup">Группа, в которой должно быть открыто окно. Если значение отсутстствует,
		/// используется значение, возвращаемое окном.</param>
		/// <returns>Возвращает ссылку на экземпляр созданного окна, или <code>null</code>,
		/// если создание окна невозможно.</returns>
		IWindow ShowWindow(string windowId, object[] args = null, bool? isUnique = null,
			bool? overlap = null, string windowGroup = null);

		/// <summary>
		/// Закрыть все окна указанного типа.
		/// </summary>
		/// <param name="args">В качестве аргументов могут выступать классы закрываемых окон
		/// или их идентификаторы.</param>
		/// <returns>Возвращает количество закрытых окон.</returns>
		int CloseAll(params object[] args);

		/// <summary>
		/// Получить окно указанного типа.
		/// </summary>
		/// <param name="arg">В качестве аргумента может выступать класс окна, или его идентификатор.</param>
		/// <returns>Возвращает первое окно указанного типа или с указанным идентификатором.</returns>
		IWindow GetWindow(object arg);

		/// <summary>
		/// Получить все окна указанного типа.
		/// </summary>
		/// <param name="args">В качестве аргументов могут выступать классы окон или их идентификаторы.</param>
		/// <returns></returns>
		IWindow[] GetWindows(params object[] args);

		/// <summary>
		/// Получить текущее открытое эксклюзивное окно из указанной группы. Если группа не указана, возвращаются
		/// все текущие открытые эксклюзивные окна из всех групп. 
		/// </summary>
		/// <param name="groupId">Идентификатор группы, для которой запрашивается окно.</param>
		/// <returns>Возвращает список открытых в настоящий момент эксклюзивных окон, или пустой список,
		/// если открытых эксклюзивных окон нет.</returns>
		IWindow[] GetCurrentUnique(string groupId = null);

		/// <summary>
		/// Поток открываемых окон.
		/// </summary>
		IObservable<IWindow> WindowOpenedStream { get; }

		/// <summary>
		/// Поток идентификаторов закрываемых методом IWindow.Close() окон.
		/// </summary>
		IObservable<string> WindowClosedStream { get; }

		/// <summary>
		/// Зарегистрировать новое окно в Менеджере.
		/// </summary>
		/// <param name="windowPrefab">Префаб нового окна.</param>
		/// <param name="overrideExisting">Флаг, указывающий перезаписать префаб окна с таким же идентификатором,
		/// если таковой уже зарегистрирован в Менеджере.</param>
		/// <returns>Возвращает <code>true</code>, если новое окно успешно зарегистрировано.</returns>
		bool RegisterWindow(Window windowPrefab, bool overrideExisting = false);

		/// <summary>
		/// Удалить регистрацию окна в Менеджере.
		/// </summary>
		/// <param name="windowId">Идентификатор удаляемого окна.</param>
		/// <returns>Возвращает <code>true</code>, если регистрация успешно удалена.</returns>
		bool UnregisterWindow(string windowId);

		/// <summary>
		/// Задать иерархию групп окон. Окна из групп, идущих первыми в списке groupHierarchy, будут отображаться
		/// под окнами, идущими последними в списке. Чем выше индекс, тем выше приоритет при отображнии.
		/// </summary>
		/// <param name="groupHierarchy">Иерархический список групп окон. Первые
		/// будут отображаться под последними.</param>
		void SetGroupHierarchy(IEnumerable<string> groupHierarchy);
	}
}